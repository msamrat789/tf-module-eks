#variable "addons" {
#  type = list(object({
#    name    = string
#    version = string
#  }))
#  default = [
#    {
#      name    = "kube-proxy"
#      version = "v1.23.7-eksbuild.1"
#    },
#    {
#      name    = "vpc-cni"
#      version = "v1.11.0-eksbuild.1"
#    },
#    {
#      name    = "coredns"
#      version = "v1.8.7-eksbuild.2"
#    },
#    {
#      name    = "aws-ebs-csi-driver"
#      version = "v1.19.0-eksbuild.2"
#    }
#  ]
#}
variable "ENV" {}
variable "PRIVATE_SUBNET_IDS" {}
variable "PUBLIC_SUBNET_IDS" {}
variable "DESIRED_SIZE" {}
variable "MAX_SIZE" {}
variable "MIN_SIZE" {}
variable "CREATE_ALB_INGRESS" {
  default = false
}
variable "CREATE_EXTERNAL_SECRETS" {
  default = false
}


variable "INSTALL_KUBE_METRICS" {
  default = true
}

variable "CREATE_SCP" {
  default = false
}

variable "CREATE_NGINX_INGRESS" {
  default = false
}

variable "CREATE_PARAMETER_STORE" {
  default = false
}